<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">

    <?php $form = ActiveForm::begin(['id'=>'myForm']); ?>

    <?= $form->field($model, 'roll')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
        
$('#myForm').on('beforeSubmit',function(e)
    {
      var \$form = $(this);
        $.post(
            \$form.attr("action"),
            \$form.serialize()
        )
        .done(function(result){
            if(result==1)
            {
                $(\$form).trigger("reset");
                $.pjax.reload({container:'#studentGrid'});
            }else
            {
                $('#message').html(result.message);
            }
        }).fail(function()
        {
           console.log("server error"); 
        });
        return false;
    });        
JS;
$this->registerJs($script);
?>