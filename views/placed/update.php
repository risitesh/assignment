<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Placed */

$this->title = 'Update Placed: ' . ' ' . $model->place_id;
$this->params['breadcrumbs'][] = ['label' => 'Placeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->place_id, 'url' => ['view', 'id' => $model->place_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="placed-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
