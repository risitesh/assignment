<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Placed */

$this->title = 'Assign Students';
$this->params['breadcrumbs'][] = ['label' => 'Placeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placed-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
