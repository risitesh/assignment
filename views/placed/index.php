<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Company;
use app\models\Student;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlacedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Placed Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placed-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Assign Students', ['value'=>  Url::to(Yii::$app->getHomeUrl().'/placed/create'),'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
    <?php
    Modal::begin([
       'header'=>'<h4>Student</h4>',
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>
    <?php Pjax::begin(['id'=>'placedGrid'])?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'place_id',
            [
                'attribute'=>'roll',
                'format'=>'raw',
                'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'size' => 'md',
                        'attribute' => 'roll',
                        'data' => ArrayHelper::map(Student::find()->all(), 'roll', 'roll'),
                        'options' => ['placeholder' => 'Select Roll No'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
            ],
            [
                'attribute'=>'name',
                'value'=>'student.name',
            ],
            [
                'attribute'=>'id',
                'value'=>'company.name',
                'format'=>'raw',
                'filter'=>Select2::widget([
                        'model' => $searchModel,
                        'size' => 'md',
                        'attribute' => 'id',
                        'data' => ArrayHelper::map(Company::find()->all(), 'name', 'name'),
                        'options' => ['placeholder' => 'Select Company'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
            ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end()?>
</div>
