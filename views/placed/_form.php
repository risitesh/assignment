<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Company;
use app\models\Student;

/* @var $this yii\web\View */
/* @var $model app\models\Placed */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="placed-form">

    <?php $form = ActiveForm::begin(['id'=>'placedForm']); ?>

    <?= $form->field($model, 'roll')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Student::find()->all(), 'roll', 'name'),
    'options' => ['placeholder' => 'Select Student'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    ])->label('Student');?>

    <?= $form->field($model, 'id')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Company::find()->all(), 'id', 'name'),
    'options' => ['placeholder' => 'Select Company'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    ])->label('Company');?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
        
$('#placedForm').on('beforeSubmit',function(e)
    {
      var \$form = $(this);
        $.post(
            \$form.attr("action"),
            \$form.serialize()
        )
        .done(function(result){
            if(result==1)
            {
                $(\$form).trigger("reset");
                $.pjax.reload({container:'#placedGrid'});
            }else
            {
                $('#message').html(result.message);
            }
        }).fail(function()
        {
           console.log("server error"); 
        });
        return false;
    });        
JS;
$this->registerJs($script);
?>