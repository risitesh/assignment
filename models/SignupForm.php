<?php
namespace app\models;

use yii\base\Model;
use Yii;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $contact;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['email','required'],
            ['email','email'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This email has already been taken.'],
            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['contact','required'],
            ['contact','integer'],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;;
            $user->email = $this->email;
            $user->contact = $this->contact;
            $user->setPassword($this->password);
            $user->generateAuthKey();
//            
//            $value = Yii::$app->mailer->compose('Online FIR')
//                    ->setFrom(['support@onlinefir.in' => 'Online FIR'])
//                    ->setTo($this->username)
//                    ->setSubject('Report Of FIR')
////                    ->setHtmlBody('Your FIR has been instituted.')
//                    ->send();
            $user->save();        
            return $user;
        }
    }
}
