<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "placed".
 *
 * @property integer $place_id
 * @property integer $roll
 * @property integer $id
 *
 * @property Student $roll0
 * @property Company $id0
 */
class Placed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'placed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roll'], 'required','message'=>'Student Name cannot be blank'],
            [['id'],'required'],
            [['roll', 'id'], 'integer'],
            [['roll'],'unique','targetClass' => 'app\models\Placed', 'message' => 'Already Placed in some company'],
//            [['roll','id'], 'unique', 'targetAttribute' => ['roll', 'id'],'message'=>'Already Placed']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'place_id' => 'Place ID',
            'roll' => 'Roll',
            'id' => 'Company',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['roll' => 'roll']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'id']);
    }
}
